# Multiple Domain Wordpress with Traefik, MariaDB, PHP-FPM, Redis and PHPMyAdmin!!

**Complete docker based multiple domain wordpress webserver**

Wondering if you can run multiple instances of Wordpress using docker technology?

Well... the answer is YES! and here is how we do it:

Here you have two folders, "traefik", and "web"

**Traefik folder**: It contains a docker-compose file to run a traefik container who´ll be responsible fo all layer 7 routing, that is: when someone wants to access wordpress1.yourdomain.com it redirects that request to the correct set of containers, and when someone wants to access wordpress2.yourdomain.com it redirects to that set of containers, and so on. It also handles the setup and renewal of letsencrypt certificates automatically

**Web folder**: It contains a docker-compose file to run the following containers: 
- NGINX
- Wordpress PHP-FPM
- MariaDB
- Redis Cache
- PHPMyAdmin

to be continued soon...

